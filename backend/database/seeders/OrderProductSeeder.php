<?php

namespace Database\Seeders;

use App\Models\OrderProduct;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        
        OrderProduct::create([
            'order_id' => 1,
            'product_id' => 1,
            'quantity' => 3
        ]);
        
        OrderProduct::create([
            'order_id' => 1,
            'product_id' => 2,
            'quantity' => 4
        ]);
        
        OrderProduct::create([
            'order_id' => 1,
            'product_id' => 3,
            'quantity' => 8
        ]);
        
        OrderProduct::create([
            'order_id' => 2,
            'product_id' => 3,
            'quantity' => 3
        ]);
        
        OrderProduct::create([
            'order_id' => 2,
            'product_id' => 4,
            'quantity' => 4
        ]);
        
        OrderProduct::create([
            'order_id' => 2,
            'product_id' => 5,
            'quantity' => 7
        ]);
    }
}
