<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::create(['name' => 'Sudadera', 'reference' => 'REF-1']);
        Product::create(['name' => 'Camisa tipo polo', 'reference' => 'REF-2']);
        Product::create(['name' => 'Sandalias', 'reference' => 'REF-3']);
        Product::create(['name' => 'Sombrero', 'reference' => 'REF-4']);
        Product::create(['name' => 'Pantalon', 'reference' => 'REF-5']);
    }
}
