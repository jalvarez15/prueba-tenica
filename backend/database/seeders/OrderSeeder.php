<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Order::create([
            'code' => '123456',
            'customer_id' => 1,
            'delivery_address' => 'Calle 80 con carrera 90',
            'delivery_date' => '2023-10-31',
            'order_state' => 'En proceso'
        ]);

        Order::create([
            'code' => '123456789',
            'customer_id' => 2,
            'delivery_address' => 'Calle 100 con carrera 70',
            'delivery_date' => '2023-12-31',
            'order_state' => 'Despachado'
        ]);
    }
}
