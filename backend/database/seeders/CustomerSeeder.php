<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Customer::create([
            'document_type' => 'Cedula de ciudadania',
            'document_number' => '123456789',
            'name' => 'Juan David Alvarez',
        ]);
        Customer::create([
            'document_type' => 'Cedula de ciudadania',
            'document_number' => '123456789',
            'name' => 'Jose David Vasquez',
        ]);
    }
}
