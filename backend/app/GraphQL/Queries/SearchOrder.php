<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Models\Order;
use Illuminate\Support\Facades\Validator;
use Nuwave\Lighthouse\Exceptions\ValidationException;

final readonly class SearchOrder
{
    /** @param  array{}  $args */
    public function __invoke(null $_, array $args)
    {

        $query = Order::query();
        $validator = Validator::make($args, [
            'code' => 'required|string',
            'document_type' => 'required|string',
            'document_number' => 'required|string',
        ]);

        if ($validator->fails()) {
            throw new ValidationException('errors', $validator);
        }

        $query = Order::query()
            ->when(isset($args['code']), function ($query) use ($args) {
                $query->where('code', $args['code']);
            });

        $order = $query->first();

        if (!$order) {
            return [
                'order' => null,
                'message' => 'No se encontró una orden con el código proporcionado.',
                'success' => false
            ];
        }

        $customer = $order->customer;

        if (
            $customer->document_type !== $args['document_type'] ||
            $customer->document_number !== $args['document_number']
        ) {
            return [
                'order' => null,
                'message' => 'El tipo de documento o número de documento no coinciden con los registros.',
                'success' => false
            ];
        }
        return [
            'order' => $order,
            'message' => 'Pedido encontrado correctamente.',
            'success' => true
        ];
    }
}
