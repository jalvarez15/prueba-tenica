<?php declare(strict_types=1);

namespace App\GraphQL\Queries;

final readonly class Products
{
    /** @param  array{}  $args */
    public function __invoke(null $_, array $args)
    {
        dd($args);
        $order = \App\Models\Order::find($args['id']);
        return $order->products();
    }
}
