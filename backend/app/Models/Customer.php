<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = ['document_type', 'document_number', 'name'];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
