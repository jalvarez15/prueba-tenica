<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'customer_id', 'delivery_address', 'delivery_date', 'order_state'];
    
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class,'order_product')->withPivot(['quantity']);
    }
    
    public function customer(): HasOne
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
