<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class OrderProduct extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'product_id', 'quantity'];

    protected $table = 'order_product';

    public $timestamps = false;
        
    // public function order(): BelongsToMany
    // {
    //     return $this->belongsToMany(Order::class);
    // }
        
    // public function product(): BelongsToMany
    // {
    //     return $this->belongsToMany(Product::class)->withPivot('quantity');
    // }
}
