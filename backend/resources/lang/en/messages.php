<?php

return [
    'Successful_Information' => 'La información se cargó con éxito!',
    'error_startdate' => 'El parámetro fechainicio no puede estar vacío o debe tener un formato de fecha adecuado',
];