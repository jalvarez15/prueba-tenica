<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Líneas de Idioma para Autenticación
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas de idioma se utilizan durante la autenticación para varios
    | mensajes que debemos mostrar al usuario. Eres libre de modificar
    | estas líneas de idioma según los requisitos de tu aplicación.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Por favor, inténtalo de nuevo en :seconds segundos.',

];