<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Líneas de Idioma para Restablecimiento de Contraseña
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas de idioma son las líneas predeterminadas que coinciden con las razones
    | proporcionadas por el gestor de contraseñas para un intento de actualización de contraseña
    | que ha fallado, como por ejemplo un token inválido o una contraseña nueva inválida.
    |
    */

    'reset' => '¡Tu contraseña ha sido restablecida!',
    'sent' => '¡Hemos enviado un enlace para restablecer tu contraseña por correo electrónico!',
    'throttled' => 'Por favor, espera antes de intentarlo de nuevo.',
    'token' => 'Este token para restablecer la contraseña es inválido.',
    'user' => "No podemos encontrar un usuario con esa dirección de correo electrónico.",

];
