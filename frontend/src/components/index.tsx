import { useState } from "react";
import ConsultaGenerada from "./consultaGenerada";


export default function Index() {

    const [order, setOrder] = useState(null);

    const submit = (e: any) => {

        e.preventDefault();
        const data = Object.fromEntries(new FormData(e.target));

        let query = `{
            "query": "query ($code: String, $document_type: String, $document_number: String) { searchOrder(code: $code, document_type: $document_type, document_number: $document_number) { order { code customer { name } delivery_address products { name reference } order_state delivery_date } message success } }",
            "variables": {
              "code": "${data.code}",
              "document_type": "${data.type_document}",
              "document_number": "${data.document}"
            }
          }`

        findOrder(query);
    };

    const findOrder = (query: string) => {
        fetch('http://127.0.0.1:8000/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: query})
        .then((response) => response.json())
        .then(({data}) => {
            if(data.searchOrder.success){
                setOrder(data.searchOrder.order);
            }else{
                alert(data.searchOrder.message);
            }
        })
        .catch(error => alert(error.toString()));
    }

    const onClose = () => {
        setOrder(null);
    }

    return (
        <main>
            {!order && (<div>
                <div className="card p-0">
                    <div className="card-header px-5 py-3">
                        <p className="m-0">Consulta el estado de tu pedido</p>
                    </div>     
                    <div className="card-body p-4">
                        <form method="POST" onSubmit={submit}>
                            <div className="mb-3 text-start">
                                <label htmlFor="code" className="form-label">Código del pedido</label>
                                <input type="text" required name="code" className="form-control" id="code"/>
                            </div>
                            <div className="mb-3 text-start">
                                <label className="form-label">Tipo de documento</label>
                                <select className="form-select" name="type_document" required>
                                    <option value="" defaultValue={""}>Seleccionar tipo de documento</option>
                                    <option value="Cedula de ciudadania">Cedula de ciudadania</option>
                                    <option value="NIT">NIT</option>
                                    <option value="Cedula de extranjeria">Cedula de extranjeria</option>
                                    <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                                </select>
                            </div>
                            <div className="mb-3 text-start">
                                <label htmlFor="document" className="form-label">Número de documento</label>
                                <input type="text" required name="document" className="form-control" id="document"/>
                            </div>
                            <button type="submit" className="btn btn-primary">Consultar</button>
                        </form>
                    </div>
                </div>
            </div>)}

            {order && (<ConsultaGenerada order={order} onClose={onClose}/>)}
        </main>


    );
}