
export default function ConsultaGenerada({order, onClose}: any) {

    const {code, delivery_address, delivery_date, order_state, products, customer} = order

    return (
        <div>
            <div className="card p-0">
                <div className="card-header px-5 py-3">
                    <p className="m-0">Estado del pedido</p>
                </div>     
                <div className="card-body p-4">
                    <div className="mb-3 text-start">
                        <p>Código del pedido: {code}</p>
                    </div>
                    <div className="mb-3 text-start">
                        <p>Cliente: {customer.name}</p>
                    </div>
                    <div className="mb-3 text-start">
                        <p>Dirección de entrega: {delivery_address}</p>
                    </div>
                    <div className="mb-3 text-start">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre del producto</th>
                                <th scope="col">Ref</th>
                                <th scope="col">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map((product: any) => (
                                <tr key={product.reference}>
                                    <td>{product.name}</td>
                                    <td>{product.reference}</td>
                                    <td></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    </div>
                    <div className="mb-3 text-start">
                        <p>Estado del pedido: {order_state}</p>
                    </div>
                    <div className="mb-3 text-start">
                        <p>Fecha estimada de entrega: {delivery_date}</p>
                    </div>
                    <button type="button" onClick={onClose} className="btn btn-danger me-2">Cerrar</button>
                    <button type="button" onClick={onClose} className="btn btn-primary">Nueva consulta</button>
                </div>
            </div>
        </div>
    );
}